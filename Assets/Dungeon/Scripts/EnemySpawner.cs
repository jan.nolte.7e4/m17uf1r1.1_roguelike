﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public List<EnemyData> enemyList;
    public GameObject enemyPrefab;
    private bool isEnemyCreated;
    public List<Sprite> possibleSpawners;
    private void Start()
    {
        isEnemyCreated = false;
        this.gameObject.GetComponent<SpriteRenderer>().sprite = possibleSpawners[Random.Range(0, possibleSpawners.ToArray().Length-1)];
    }
    public void spawnEnemy()
    {
        if (!isEnemyCreated)
        {
            GameObject drop = Instantiate(enemyPrefab, transform.position, new Quaternion(0,0,0,0));
            drop.GetComponent<zombiMovement>().enemyData = nextSpwan();
            if (drop.GetComponent<zombiMovement>())
            {
                drop.GetComponent<zombiMovement>().spwanerParent = this.gameObject;
            }
            else
            {
                drop.GetComponent<EnemyShooting>().spwanerParent = this.gameObject;
            }
            
            isEnemyCreated = true;
        }
    }
    public void resetSpawnEnemy()
    {
        isEnemyCreated = false;
    }
    private EnemyData nextSpwan()
    {
        return enemyList[Random.Range(0, enemyList.ToArray().Length)];
    }
}
