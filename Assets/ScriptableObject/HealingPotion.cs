﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HealingPotion", menuName = "ScriptableObjects/Items/Consumables/HealingPotion", order = 1)]
public class HealingPotion : Consumables
{
    public float healing;
    public override void EffectOnPlayer(GameObject player)
    {
        player.GetComponent<PlayerData>().heal(healing);
    }
}
