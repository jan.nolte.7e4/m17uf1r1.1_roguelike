﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Ammo", menuName = "ScriptableObjects/Items/Consumables/Ammo", order = 1)]
public class Ammo : Consumables
{
    public int reestockBullets;
    public override void EffectOnPlayer(GameObject player)
    {
        player.GetComponent<PlayerData>().earnBullets(reestockBullets);
    }
}
