﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Weapon", menuName = "ScriptableObjects/Items/Active/Weapons", order = 1)]
public class Weapons : Items
{
    public Sprite imageWeapon;
    public int bulletsShoot;
    public float shootSpreat;
    public int magazine;
    public float damageMultiplier;
    public int bulletsSpend;

    public void EquipPlayer(GameObject player)
    {
        player.GetComponent<PlayerData>().addWeapon(this);
    }
}
