﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponMove : MonoBehaviour
{
    [SerializeField]
    public Weapons weapon;
    public GameObject prefab;
    public GameObject weaponShoot;
    private bool shooting;
    void Start()
    {
        shooting = false;
    }

    void Update()
    {
        this.gameObject.GetComponent<SpriteRenderer>().sprite = weapon.imageWeapon;
        if (Input.GetMouseButtonDown(0))
        {
            if (this.gameObject.GetComponentInParent<PlayerData>().ableToShoot(weapon.bulletsSpend) && !shooting)
            {
                StartCoroutine(shootingTime(weapon.bulletsShoot, weapon.shootSpreat));
            }
            else
            {
                Debug.Log("No ammo left");
            }
            
        }
    }
    private IEnumerator shootingTime(int bullets, float spread)
    {
        shooting = true;
        for(int x = 0; x < bullets; x++)
        {   
            GameObject bullet = Instantiate(prefab, transform.position, new Quaternion(0, 0, 0, 0));
            Debug.Log(1 * weapon.damageMultiplier * this.gameObject.GetComponentInParent<PlayerData>().getDMGMultiplier());
            if (weapon.itemName.Equals("Pistol_Upgraded"))
            {
                bullet.GetComponent<BulletMove>().setStunner(true);
            }
            else
            {
                bullet.GetComponent<BulletMove>().setStunner(false);
            }
            bullet.GetComponent<BulletMove>().setBulletDamage(1*weapon.damageMultiplier*this.gameObject.GetComponentInParent<PlayerData>().getDMGMultiplier());
            bullet.GetComponent<Rigidbody2D>().AddForce(weaponShoot.transform.right * 1, ForceMode2D.Impulse);
            this.gameObject.GetComponentInParent<PlayerData>().loseBullets(weapon.bulletsSpend);
            yield return new WaitForSeconds(0.2f);
        }
        shooting = false;
    }    
}
