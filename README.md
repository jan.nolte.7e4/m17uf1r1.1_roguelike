Esquema de Contingut:
    - 4 Armes diferents. 1 de elles estuneja als enemics.
    - 3 Mapas diferents que apareixen aleatoriament.
    - 1 Tenda on es poden comprar caixes amb items.
    - 3 Consumibles de bales i 1 de poció sanadora.
    - Sistema de monedes per a comprar els cofres.
    - 10 modificadors diferents.
    - 1 clase d'inici pel jugador (amb possibilitat d'afegir dues més). 
    - Cada planta s'ha de sobreviure un temps determinat per poder passar a la següent.
Controls:
    - AWSD o Arrows -> Moviment del personatge.
    - C -> Canvi d'arma.
    - Q -> Mostrar informació del jugador.
    - Ratolí per apuntar.  
