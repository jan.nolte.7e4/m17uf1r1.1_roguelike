﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerControllerEnemies : MonoBehaviour
{
    public List<GameObject> spwaners;
    private void Start()
    {
        StartCoroutine("Spawning");
    }
    private IEnumerator Spawning()
    {
        while (true)
        {
            foreach (GameObject spawner in spwaners)
            {
                if (Random.Range(0, 100) > 80)
                {
                    spawner.GetComponent<EnemySpawner>().spawnEnemy();
                }
            }
            yield return new WaitForSeconds(1f);
        }

    }
}
