﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static int _score;
    public static int Score
    {
        get
        {
            return _score;
        }
    }
    private static int highScore;
    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    /*{
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
        DontDestroyOnLoad(this);
    }*/
    {
        //TODO: highScore from database
        
        if (_instance == null)         
        {             
            DontDestroyOnLoad(gameObject); 
            _instance = this;          
        }         
        else { 
            Destroy(gameObject);
        }     
    }

    // Start is called before the first frame update
    void Start()
    {
        highScore = getSavedHScore();
        _score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(playerMovementState());
        /*switch (SceneManager.GetActiveScene().name)
        {
            case "Game":
                Debug.Log("You are in Game;");
                UI.GetComponent<UIController>().hpPlayerBar.value = (player) ? player.GetComponent<PlayerData>().howmuchHP() : 0;
                UI.GetComponent<UIController>().ammoText.text = "Ammo left: " + ((player) ? player.GetComponent<PlayerData>().numberAmmoLeft() : 0).ToString();
                if (!GameObject.FindGameObjectWithTag("Player")) SceneManager.LoadScene("Score");
                break;
            case "Score":
                Debug.Log("You are in Score;");
                Debug.Log(Score);
                UI.GetComponent<UIController>().score.text = "Score: " + (Score.ToString());
                break;
            case "Menu":
                Debug.Log("You are in Menu;");
                break;
        }*/


    }
    public int getScore()
    {
        Debug.Log("Score: " + Score);
        return Score;
    }
    public int getHScore()
    {
        Debug.Log("HScore: " + highScore);
        return highScore;
    }
    
    public void modifyHScore()
    {
        Debug.Log(Score);
        if (Score > highScore)
        {
            highScore = Score;
            saveHScore();
        }
    }
    public void saveHScore()
    {
        PlayerPrefs.SetInt("HighScore", highScore);
    }
    public int getSavedHScore()
    {
        return PlayerPrefs.GetInt("HighScore");
    }
    public void addScore(int? value)
    {
        if (value.HasValue)
        {
            _score += value.Value;
        }
        else
        {
            _score += 5;
        }
        Debug.Log("Score: "+Score);
    }
    public void resetScore()
    {
        _score = 0;
    }
    public void gameOver()
    {
        SceneManager.LoadScene("Score");
        modifyHScore();
    }
    public void closeGame()
    {
        saveHScore();
        Application.Quit();
    }
}
