﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemSpawner : MonoBehaviour
{
    public List<Consumables> itemList;
    public GameObject prefabDrop;
    private bool isItemThere;
    private void Start()
    {
        isItemThere = false;
    }
    public void spawnItem()
    {
        if (!isItemThere)
        {
            GameObject drop = Instantiate(prefabDrop, transform.position, new Quaternion(0, 0, 0, 0));
            drop.GetComponent<ItemHandling>().spwanerParent = this.gameObject;           
            drop.GetComponent<ItemHandling>().itemDrop = nextDrop();           
            isItemThere = true;
        }
    }
    public void resetSpawnItem()
    {
        isItemThere = false;
    }
    private Consumables nextDrop()
    {
        return itemList[Random.Range(0, itemList.ToArray().Length)];
    }
}
