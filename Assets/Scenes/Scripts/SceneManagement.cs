﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : MonoBehaviour
{

    private Scenes _currentScene;

    public enum Scenes
    {
        Menu, Achievements, Game, Score
    }

    public Scenes CurrentScene
    {
        get => _currentScene;
        set
        {
            _currentScene = value;
            ChangeScene();
        }
    }

    private void Start()
    {
        Scene scene = SceneManager.GetActiveScene();
        string activeScene;
        activeScene = scene.name;
        _currentScene = (Scenes)System.Enum.Parse(typeof(Scenes), activeScene);
    }

    void ChangeScene()
    {
        Debug.Log("SCENE CHANGED");
        switch (_currentScene)
        {
            case Scenes.Menu:
                JumpToMenu();
                break;
            case Scenes.Achievements:
                JumpToAchivements();
                break;
            case Scenes.Game:
                JumpToGame();
                break;
            case Scenes.Score:
                JumpToScore();
                break;
        }
    }

    void JumpToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    void JumpToAchivements()
    {
        SceneManager.LoadScene("Achievements");
    }

    void JumpToGame()
    {
        SceneManager.LoadScene("Game");
        GameManager.Instance.resetScore();
    }

    void JumpToScore()
    {
        SceneManager.LoadScene("Score");
    }
}
