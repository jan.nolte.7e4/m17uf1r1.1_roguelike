﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullets : MonoBehaviour
{
    private float bulletDamage;
    private GameObject shooter;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerData>().loseHP(bulletDamage);
            Destroy(this.gameObject);
        }
        else if(collision.gameObject!=shooter && !collision.gameObject.CompareTag("Money"))
        {
            Destroy(this.gameObject);
        }     
    }
    public void setBulletDamage(float dmg)
    {
        bulletDamage = dmg;
    }
    public void setShooter(GameObject _shooter)
    {
        this.shooter = _shooter;
    }
}
