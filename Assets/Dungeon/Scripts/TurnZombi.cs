﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnZombi : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Vector3 direc = player.transform.position - transform.position;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(direc.y, direc.x) * Mathf.Rad2Deg));
    }
}
