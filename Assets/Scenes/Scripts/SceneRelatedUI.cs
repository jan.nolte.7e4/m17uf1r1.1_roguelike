﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneRelatedUI : MonoBehaviour
{
    SceneManagement scene = new SceneManagement();

    public void LoadMenu()
    {
        scene.CurrentScene = SceneManagement.Scenes.Menu;
    }

    public void LoadGame()
    {
        //Debug.Log("GamePressingBUTTOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOON");
        scene.CurrentScene = SceneManagement.Scenes.Game;
    }

    public void LoadScore()
    {
        scene.CurrentScene = SceneManagement.Scenes.Score;
    }
}
