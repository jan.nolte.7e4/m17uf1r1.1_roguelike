﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BoxHandling : MonoBehaviour
{
    public List<Items> dropableItems;
    private float startingHP = 3;
    private float currentHP;
    public GameObject itemPrefab;
    public int price = 0;
    public TextMeshPro valueText;
    private string boxDescription="";
    void Start()
    {
        currentHP = startingHP;
        valueText.text = price.ToString();
    }
    public void hitToTheBox(float dmg)
    {
        currentHP -= dmg;
        Debug.Log(currentHP);
        if (currentHP<= 0)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerData pData = collision.gameObject.GetComponent<PlayerData>();
            GameObject newObject = Instantiate(itemPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
            newObject.GetComponent<ObjectInteraction>().item = dropableItems[Random.Range(0, dropableItems.ToArray().Length)];
            pData.spendMoney(price);
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerData pData = collision.GetComponent<PlayerData>();
            if (!pData.canSpendMoney(price))
            {
                collision.transform.position = new Vector3(0, 0, 0);
            }
            GameObject.Find("Canvas").GetComponent<UIController>().showInfoBox(boxDescription);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameObject.Find("Canvas").GetComponent<UIController>().showInfoBox("");
        }
    }
    public void setTextToShow(string text)
    {
        boxDescription = text;
    }
}
