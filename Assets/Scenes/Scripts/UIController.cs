﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class UIController : MonoBehaviour
{
    public Slider hpPlayerBar;
    public Text ammoText;
    public Text score;
    public Text highScore;
    public Text coins;
    public Text timer;
    public Text boxInfo;
    public Text gameInfo;
    public Text playerDetails;
    private PlayerData pd;
    private MapCreator mc;
    private void Start()
    {
        pd = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerData>();
        mc = GameObject.Find("Map").GetComponent<MapCreator>();
        if(SceneManager.GetActiveScene().name == "Game")
        {
            boxInfo.text = "";
        }
        if(SceneManager.GetActiveScene().name == "Score")
        {
            if (GameManager.Instance.getScore() == GameManager.Instance.getHScore())
            {
                StartCoroutine(BlinquingScore());
            }
        }
    }
    private void Update()
    {
        switch (SceneManager.GetActiveScene().name)
        {
            case "Game":
                /*PlayerData pd = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerData>();
                MapCreator mc = GameObject.Find("Map").GetComponent<MapCreator>();*/
                hpPlayerBar.value = (pd) ? pd.howmuchHP()*10 : 0;
                hpPlayerBar.maxValue = pd.getMaxHP()*10;
                ammoText.text = "Ammo left: " + ((pd) ? pd.numberAmmoLeft() : 0).ToString();
                score.text = "Score: " + (GameManager.Instance.getScore().ToString());
                coins.text = "Coins: " + pd.getCoins().ToString();
                if (Input.GetKey(KeyCode.Q))
                {
                    playerDetails.text = "Details:\n" + pd.getDetails();
                }
                else
                {
                    playerDetails.text = "";
                }
                if (mc.inshop())
                {
                    timer.text = "";
                    gameInfo.text = "";
                }
                else
                {
                    timer.text = ((int)mc.getTime() / 60).ToString() + " : " + ((mc.getTime() % 60 <= 9) ? "0" : "") + (mc.getTime() % 60).ToString("f2");
                    if (!mc.door.active) {
                        gameInfo.text = "Survive for:" + ((int)mc.timeToSurvive() / 60).ToString() + " : " + ((mc.timeToSurvive() % 60 <= 9) ? "0" : "") + (mc.timeToSurvive() % 60).ToString("f2");
                    }
                    else
                    {
                        gameInfo.text = "Go to the Exit";
                    }
                }
                
                break;
            case "Score":
                //Debug.Log("Score Screen;");
                
                score.text = "Score: " + (GameManager.Instance.getScore().ToString());
                highScore.text = "HighScore: " + (GameManager.Instance.getHScore().ToString());
                break;
            case "Menu":
                //Debug.Log("You are in Menu;");
                break;
        }
    }
    public void showInfoBox(string textToShow)
    {
        boxInfo.text = textToShow;
    }
    private IEnumerator BlinquingScore()
    {
        while (true)
        {
            score.color = Color.red;
            yield return new WaitForSeconds(0.5f);
            score.color = Color.yellow;
            yield return new WaitForSeconds(0.5f);
            score.color = Color.green;
            yield return new WaitForSeconds(0.5f);
        }
    }
}
