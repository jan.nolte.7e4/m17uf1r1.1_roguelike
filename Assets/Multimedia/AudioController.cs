﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioController : MonoBehaviour
{
    public AudioClip menuMusic;
    public AudioClip game;
    public AudioClip shop;

    void Start()
    {
        switch (SceneManager.GetActiveScene().name)
        {
            case "Game":
                this.gameObject.GetComponent<AudioSource>().clip = game;
                this.gameObject.GetComponent<AudioSource>().Play();
                break;
            case "Menu":
                this.gameObject.GetComponent<AudioSource>().clip = menuMusic;
                this.gameObject.GetComponent<AudioSource>().Play();
                break;
            case "Score":
                this.gameObject.GetComponent<AudioSource>().clip = shop;
                this.gameObject.GetComponent<AudioSource>().Play();
                break;

        }
    }

}
