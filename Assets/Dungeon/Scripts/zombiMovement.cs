using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zombiMovement : MonoBehaviour
{
    public GameObject spwanerParent;
    private float hp;
    private float movementSpeed;
    public List<Coins> dropableCoins;
    public GameObject prefabDrops;
    public EnemyData enemyData;
    private bool stun;
    void Start()
    {
        this.gameObject.GetComponent<SpriteRenderer>().color = enemyData.enemyColor;
        hp = enemyData.hpStarting;/* + GameObject.Find("Map").GetComponent<MapCreator>().floorModifierHP();*/
        movementSpeed = enemyData.movementStarting; 
        stun = false;
        if (enemyData.enemyClass.Equals(EnemyData.classEnemy.Shooter))
        {
            InvokeRepeating("shootPlayer", enemyData.cadenceShoot, 1);
        }
    }

    

    // Update is called once per frame
    void Update()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player!=null && !stun)
        {
            Vector3 direc = player.transform.position - transform.position;
            this.transform.rotation = Quaternion.Euler(new Vector3(0, 0, (Mathf.Atan2(direc.y, direc.x) * Mathf.Rad2Deg) + 90));
            this.transform.position = Vector3.MoveTowards(transform.position, player.transform.position, movementSpeed * Time.deltaTime);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerData>().loseHP(enemyData.dmgToPlayer* collision.gameObject.GetComponent<PlayerData>().getDMGFromEnemies()); 
            
            if (!collision.gameObject.GetComponent<MovementPlayer>().isInmortal())
            {
                Destroy(this.gameObject);               
            }
           
        }
    }
    public void loseHP(float val)
    {
        hp -= val;
        if (hp<=0)
        {
            GameManager.Instance.addScore(enemyData.scoreEarn);
            dropMoney();
            Destroy(this.gameObject);
        }
    }
    private void dropMoney()
    {
        GameObject newObject = Instantiate(prefabDrops, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
        int coinToDrop = Random.Range(0, 100);
        int topD = 90;
        int lowD = 60;
        Debug.Log("CointoDrop: " + coinToDrop);
        switch (coinToDrop)
        {
            case int n when (n < lowD):
                newObject.GetComponent<ObjectInteraction>().item = dropableCoins[0];
                break;
            case int n when (n >= lowD && n<=topD):
                newObject.GetComponent<ObjectInteraction>().item = dropableCoins[1];
                break;
            case int n when (n > topD):
                newObject.GetComponent<ObjectInteraction>().item = dropableCoins[2];
                break;
        }
        newObject.tag = "Money";
    }
    private void OnDestroy()
    {
        spwanerParent.GetComponent<EnemySpawner>().resetSpawnEnemy();
    }
    public void shootPlayer()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            GameObject bullet = Instantiate(enemyData.prefabBullet, this.transform.position, new Quaternion(0, 0, 0, 0));
            bullet.GetComponent<EnemyBullets>().setBulletDamage(enemyData.dmgToPlayer);
            bullet.GetComponent<EnemyBullets>().setShooter(this.gameObject);
            bullet.GetComponent<Rigidbody2D>().AddForce(this.transform.up *-1* enemyData.bulletSpeed, ForceMode2D.Impulse);
        }
    }
    public void stunned(float v)
    {
        stun = true;
        StartCoroutine(Stun(v));
    }
    private IEnumerator Stun(float time)
    {
        float savemovementSpeed = movementSpeed;
        movementSpeed = 0;
        yield return new WaitForSeconds(time);
        movementSpeed = savemovementSpeed;
        stun = false;
    }
}
