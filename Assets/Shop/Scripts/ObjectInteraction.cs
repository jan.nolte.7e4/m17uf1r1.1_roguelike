﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInteraction : MonoBehaviour
{
    public Items item;
    void Start()
    {
        this.gameObject.GetComponent<SpriteRenderer>().sprite = item.icon;
        if(item.itemType == Items.Type.Coins)
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = ((Coins) item).coinColor;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
            switch (item.itemType)
            {
                case Items.Type.Weapon:
                    Weapons w = (Weapons)item;
                    w.EquipPlayer(collision.gameObject);
                    break;
                case Items.Type.Consumable:
                    Consumables c = (Consumables)item;
                    c.EffectOnPlayer(collision.gameObject);
                    break;
                case Items.Type.Modifier:
                    Modifiers m = (Modifiers)item;
                    m.effectOnPlayer(collision.gameObject);
                    break;
                case Items.Type.Coins:
                    Coins coins = (Coins)item;
                    coins.addCoinToPlayer(collision.gameObject);
                    break;
                default:
                    Debug.Log("Error");
                    break;
            }
        }
    }
}
