﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeFloor : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            gameObject.GetComponentInParent<MapCreator>().changeMap();
            collision.gameObject.transform.position = new Vector3(0, 0, 0);
            Destroy(this.gameObject.transform.parent.gameObject);
            foreach (GameObject b in GameObject.FindGameObjectsWithTag("Box"))
            {
                Destroy(b);
            }
        }
    }
}
