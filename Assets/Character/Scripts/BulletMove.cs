﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    private float bulletDamage;
    public bool stunner;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.gameObject.tag);
        if (collision.gameObject.CompareTag("BulletDestroyer"))
        {
            Destroy(this.gameObject);
        }
        else if(collision.gameObject.CompareTag("Enemy"))
        {
            if (stunner)
            {
                collision.gameObject.GetComponent<zombiMovement>().stunned(1);
            }
            collision.gameObject.GetComponent<zombiMovement>().loseHP(bulletDamage);
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.CompareTag("Box"))
        {
            collision.gameObject.GetComponent<BoxHandling>().hitToTheBox(bulletDamage);
            Destroy(this.gameObject);            
        }
        else if (collision.gameObject.CompareTag("EnemyBullets"))
        {            
            Destroy(this.gameObject);            
        }
    }
    public void setBulletDamage(float dmg)
    {
        bulletDamage = dmg;
    }
    public float getDMG()
    {
        return bulletDamage;
    }
    public void setStunner(bool s)
    {
        stunner = s;
    }
}
