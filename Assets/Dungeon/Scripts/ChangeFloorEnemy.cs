﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeFloorEnemy : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            gameObject.GetComponentInParent<MapCreator>().changeMap();
            collision.gameObject.transform.position = new Vector3(0, 0, 0);
            Destroy(this.gameObject.transform.parent.gameObject);
            clearEarnings();
            clearEnemy();
            clearBullets();
            GameManager.Instance.addScore(250);
        }
    }
    private void clearEnemy()
    {
        foreach (GameObject b in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Destroy(b);
        }
    }
    private void clearEarnings()
    {
        foreach (GameObject b in GameObject.FindGameObjectsWithTag("Money"))
        {
            Destroy(b);
        }
    }
    private void clearBullets()
    {
        foreach (GameObject b in GameObject.FindGameObjectsWithTag("EnemyBullets"))
        {
            Destroy(b);
        }
    }

}
