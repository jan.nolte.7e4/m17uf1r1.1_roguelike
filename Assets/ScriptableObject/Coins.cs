﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Coins", menuName = "ScriptableObjects/Items/Coins", order = 1)]
public class Coins : Items
{
    public Color coinColor;
    public int earnings;
    public void addCoinToPlayer(GameObject player)
    {
        player.GetComponent<PlayerData>().addCoins(earnings);
    }
}
