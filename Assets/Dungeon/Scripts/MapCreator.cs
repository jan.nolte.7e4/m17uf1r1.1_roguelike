﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCreator : MonoBehaviour
{
    public Camera mainCamera;
    public List<Map> availableMaps;
    private bool lastWasAShop;
    [SerializeField]
    private Map currentMap;
    private int floorCount;
    private int enemyFloor;
    public GameObject door;
    private bool opendoor;
    private float closeDoor;
    private void Start()
    {
        lastWasAShop = true;
        floorCount = 0;
        enemyFloor = 0;
        mainCamera.GetComponent<CamaraFollower>().adaptBounds(new Vector4(currentMap.minX, currentMap.minY, currentMap.maxX, currentMap.maxY));
    }
    private void Update()
    {
        if (!inshop())
        {
            if (getTime() >= timeToSurvive())
            {               
                door.SetActive(true);
            }
            else
            {
                door.SetActive(false);
            }
        }
    }
    public void changeMap()
    {
        bool ready = false;
        //.name.Substring(0, 4).ToLower().Equals("shop")
       
        do {
            currentMap = rerollMap();            
             Debug.Log("lasWasAShop "+lastWasAShop +" thisisashop" + inshop());
            if (inshop())
            {
                if (lastWasAShop)
                {
                    ready = false;
                }
                else
                {
                    ready = true;
                }
            }
            else
            {
                if (enemyFloor>1)
                {
                   ready = false;
                }
                else
                {
                   ready = true;
                }
            }

        } while (!ready);
        GameObject mapObject = Instantiate(currentMap.prefabMap, currentMap.startPosition, Quaternion.identity);
        mapObject.transform.SetParent(gameObject.transform);
        door = mapObject.transform.GetChild(3).gameObject;
        closeDoor = Time.time;
        mainCamera.GetComponent<CamaraFollower>().adaptBounds(new Vector4(currentMap.minX, currentMap.minY, currentMap.maxX, currentMap.maxY));
        if (!inshop())
        {

            lastWasAShop = false;
            floorCount++;
            enemyFloor++;
        }
        else
        {
            enemyFloor = 0;
            lastWasAShop = true;
        }
        Debug.Log(enemyFloor);
    }
    public Map rerollMap()
    {       
        return availableMaps[Random.Range(0, availableMaps.ToArray().Length)];
    }
    public float getTime()
    {
        return Time.time - closeDoor;
    }
    public bool inshop()
    {
        return currentMap.name.Substring(0, 4).ToLower().Equals("shop");
    }
    public float timeToSurvive()
    {
        return 5 * floorCount;
    }
    public float floorModifierHP()
    {
        if (floorCount>5)
        {
            return (floorCount * 10) / 2;
        }
        return 0;
    }
}
