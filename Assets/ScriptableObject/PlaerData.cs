﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "PlayerData", menuName = "ScriptableObjects/Entity/PlayerData", order = 1)]
public class PlaerData : EntityData
{
    public classPlayer playerClass;
    public int coinsStarting;
    public int bulletsStarting;
    public List<Weapons> weaponsStarting;
    public float dmgMultiplierStarting;
    public float dmgFromEnemies;
    public float dashInvulnerability;
    public float cooldownDash;
    public float healingPower;
    //public List<Modifiers> modStarting;


    public enum classPlayer
    {
        Sorcerer,
        Gunner,
        Tank
    }
}
