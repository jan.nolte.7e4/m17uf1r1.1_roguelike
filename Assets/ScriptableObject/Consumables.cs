﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Consumables : Items
{
    public abstract void EffectOnPlayer(GameObject player);
}
