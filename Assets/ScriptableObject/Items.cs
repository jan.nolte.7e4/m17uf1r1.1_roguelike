﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : ScriptableObject
{
    public string itemName;
    public Sprite icon;
    public rarity chance;
    //public int value;
    public Type itemType;
    public string description;
    public enum rarity
    {
        Coins,
        Common,
        Uncommon,
        Rare,
        Legendary
    }
    public enum Type
    {
        Consumable,
        Weapon,
        Modifier, 
        Coins
    }
}
