﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopManager : MonoBehaviour
{
    public List<GameObject> boxSpawners;
    public List<Items> dropableItemsShop;
    public GameObject boxPrefab;
    void Start()
    {
        generateDrops();
    }
    private void generateDrops()
    {
        foreach (GameObject spawner in boxSpawners)
        {
            GameObject box = Instantiate(boxPrefab, new Vector3(spawner.transform.position.x, spawner.transform.position.y, spawner.transform.position.z), Quaternion.identity);
            BoxHandling boxH = box.GetComponent<BoxHandling>();
            int boxRarity = Random.Range(0, 100);
            switch (boxRarity)
            {
                case int n when (n < 20):
                    boxH.dropableItems = boxDropableItems(new Items.rarity[] { Items.rarity.Common }, new Items.Type[] { Items.Type.Consumable });
                    boxH.price = 5;
                    boxH.setTextToShow("Dropable Items.\nRarity:\n· Common\nType:\n· Consumables");
                    break;
                case int n when (n > 20 && n < 70):
                    boxH.dropableItems = boxDropableItems(new Items.rarity[] { Items.rarity.Common, Items.rarity.Uncommon }, new Items.Type[] { Items.Type.Consumable, Items.Type.Weapon });
                    boxH.price = 10;
                    boxH.setTextToShow("Dropable Items.\nRarity:\n· Common\n· Uncommon\nType:\n· Consumables\n· Weapon");
                    break;
                case int n when (n > 70 && n < 90):
                    boxH.dropableItems = boxDropableItems(new Items.rarity[] { Items.rarity.Uncommon, Items.rarity.Rare }, new Items.Type[] { Items.Type.Modifier, Items.Type.Weapon });
                    boxH.price = 15;
                    boxH.setTextToShow("Dropable Items.\nRarity:\n· Uncommon\n· Rare\nType:\n· Weapon\n· Modifier");
                    break;
                case int n when (n > 90):
                    boxH.dropableItems = boxDropableItems(new Items.rarity[] { Items.rarity.Rare, Items.rarity.Legendary }, new Items.Type[] { Items.Type.Modifier });
                    boxH.price = 20;
                    boxH.setTextToShow("Dropable Items.\nRarity:\n· Rare\n· Legendary\nType:\n· Modifier");
                    break;
                default:
                    boxH.dropableItems = boxDropableItems(new Items.rarity[] { Items.rarity.Common }, new Items.Type[] { Items.Type.Consumable });
                    boxH.price = 5;
                    boxH.setTextToShow("Dropable Items.\nRarity:\n· Common\nType:\n· Consumables");
                    break;
            }
        }
    }
    private List<Items> boxDropableItems(Items.rarity[] r, Items.Type[] t)
    {
        List<Items> drop = new List<Items>();
        foreach (Items i in dropableItemsShop)
        {
            if(searchInArray(r, i.chance) && searchInArray(t, i.itemType))
            {
                drop.Add(i);
            }
        }
        /*foreach (Items i in drop) 
        {
            Debug.Log(i);
        }*/
        return drop;
    }
    private bool searchInArray(Items.rarity[] r, Items.rarity rtoSearch)
    {
        foreach(Items.rarity rarity in r)
        {
            if (rarity.Equals(rtoSearch))
            {
                return true;
            }
        }
        return false;
    }
    private bool searchInArray(Items.Type[] t, Items.Type ttoSearch)
    {
        foreach (Items.Type type in t)
        {
            if (type.Equals(ttoSearch))
            {
                return true;
            }
        }
        return false;
    }
}
