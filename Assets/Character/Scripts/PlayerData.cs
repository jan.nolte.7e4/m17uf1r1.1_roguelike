﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public List<Weapons> weapons;
    private int id;
    private int numBullets;
    private float hp;
    private float maxHP;
    private int coins;
    private float damageMultiplier;
    public PlaerData initialInfo;
    private float enemyDMGMultiplier;
    private float healingPower;

    private void Awake()
    {
        numBullets = initialInfo.bulletsStarting;
        maxHP = initialInfo.hpStarting;
        setuopStartingWeapons(initialInfo.weaponsStarting);
        id = 0;
        coins = initialInfo.coinsStarting;
        damageMultiplier = initialInfo.dmgMultiplierStarting;
        enemyDMGMultiplier = initialInfo.dmgFromEnemies;
        healingPower = initialInfo.healingPower;
        this.gameObject.GetComponentInChildren<WeaponMove>().weapon = weapons[id];
    }
    void Start()
    {
        hp = maxHP;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.X) || Input.GetKeyDown(KeyCode.C))
        {
            changeWeapon();            
        }
    }
    public void loseBullets(int? amount)
    {
        if (amount.HasValue)
        {
            numBullets -= amount.Value;
        }
        else
        {
            numBullets -= 1;
        }
        //Debug.Log(numBullets);
    }
    public void earnBullets(int amount)
    {
        numBullets += amount;
        Debug.Log(numBullets);
    }


    public bool ableToShoot(int ammoLose)
    {
        if (numBullets - ammoLose < 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    } 
    public void loseHP(float? lhp)
    {
        if (!this.gameObject.GetComponent<MovementPlayer>().isInmortal()) {
            StartCoroutine("GettingDamage");
            if (lhp.HasValue && (lhp.Value * enemyDMGMultiplier)>0)
            {
                hp -= (lhp.Value*enemyDMGMultiplier);
            }
            else
            {
                hp -= 1f;
            }
            if (hp <= 0)
            {
                Destroy(this.gameObject);
                GameManager.Instance.gameOver();
            }
        }
    }
    public void changeWeapon()
    {
        id++;
        if (id >= weapons.ToArray().Length)
        {
            id = 0;
        }
        this.gameObject.GetComponentInChildren<WeaponMove>().weapon = weapons[id];
    }
    public float howmuchHP()
    {
        return hp;
    }
    public int numberAmmoLeft()
    {
        return numBullets;
    }
    public void addWeapon(Weapons w)
    {
        bool gotit = false;
        foreach (Weapons weapon in weapons)
        {
            if (weapon.name.Equals(w.name))
            {
                gotit = true;
                earnBullets(w.magazine*2);
                break;
            }
        }
        if (!gotit)
        {
            weapons.Add(w);
            earnBullets(w.magazine);
        }
    }
    public bool canSpendMoney(int amount)
    {      
        return amount <= coins;
    }
    public void spendMoney(int amount)
    {
        coins -= amount;
    }
    public void addCoins(int amount)
    {
        coins += amount;
    }
    public int getCoins()
    {
        return coins;
    }
    public void heal(float healing)
    {
        hp += healing*healingPower;
        if (hp > maxHP)
        {
            hp = maxHP;
        }
    }
    public void moreMaxHP(float moreHP)
    {
        maxHP += moreHP;
        hp += moreHP;
    }
    public float getMaxHP()
    {
        return maxHP;
    }
    public float getDMGMultiplier()
    {
        return damageMultiplier;
    }
    public float getDMGFromEnemies()
    {
        return enemyDMGMultiplier;
    }
    private void setuopStartingWeapons(List<Weapons> wl)
    {
        foreach(Weapons w in wl)
        {
            addWeapon(w);
        }
        
    }
    public IEnumerator GettingDamage() {
        for (int x = 0; x < 2; x++)
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
            yield return new WaitForSeconds(0.2f);
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
            if (x == 0)
            {
                yield return new WaitForSeconds(0.2f);
            }
        }
       
    }
    public void moreDMGMultiplayer(float amount)
    {
        damageMultiplier += amount;
    }
    public void moreDMGReduction(float amount)
    {
        enemyDMGMultiplier += amount;
    }
    public void moreHealingPower(float amount)
    {
        healingPower += amount;
    }
    public string getDetails()
    {
        return "DMG_UP: " + damageMultiplier.ToString("f1") + "\nDEF_UP: " + enemyDMGMultiplier.ToString("f1") + "\nHEAL_Power: " + healingPower.ToString("f1") + this.gameObject.GetComponent<MovementPlayer>().getDetails();
    }
}
