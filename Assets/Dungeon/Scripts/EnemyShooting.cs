﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    public GameObject prefab;
    public GameObject shootPoint;
    public GameObject spwanerParent;
    void Start()
    {
        InvokeRepeating("shootPlayer", 0.5f, 1);
    }

    // Update is called once per frame
    void Update()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            Vector3 direc = player.transform.position - transform.position;
            this.transform.rotation = Quaternion.Euler(new Vector3(0, 0, (Mathf.Atan2(direc.y, direc.x) * Mathf.Rad2Deg)-90));
        }
    }
    private void shootPlayer()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            GameObject bullet = Instantiate(prefab, shootPoint.transform.position, new Quaternion(0, 0, 0, 0));
            bullet.GetComponent<Rigidbody2D>().AddForce(shootPoint.transform.up * 6, ForceMode2D.Impulse);
        }
    }
    private void OnDestroy()
    {
        GameManager.Instance.addScore(10);
        spwanerParent.GetComponent<EnemySpawner>().resetSpawnEnemy();
    }
}
