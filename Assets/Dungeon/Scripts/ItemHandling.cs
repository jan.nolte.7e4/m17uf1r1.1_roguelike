﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHandling : MonoBehaviour
{
    [SerializeField]
    public Consumables itemDrop;
    public GameObject spwanerParent;
    public void setDrop(Consumables consum)
    {
        this.itemDrop = consum;
    } 
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {           
            itemDrop.EffectOnPlayer(collision.gameObject);
            Destroy(this.gameObject);
            spwanerParent.GetComponent<itemSpawner>().resetSpawnItem();
        }
    }
}
