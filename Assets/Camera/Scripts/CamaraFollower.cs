﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraFollower : MonoBehaviour
{
    [SerializeField]
    private GameObject playerObject;
    private Transform playerPos;
    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;
    private Vector2 limitsMin;
    private Vector2 limitsMax;

    void Start()
    {
        playerPos = playerObject.transform;
        
    }
    void LateUpdate()
    {
        /*if (playerPos.position.y > transform.position.y + 0.5f || playerPos.position.y < transform.position.y - 0.5f || playerPos.position.x > transform.position.x + 0.5f || playerPos.position.x < transform.position.x - 0.5f)
        {
            Vector3 targetPosition = new Vector3(transform.position.x, playerPos.position.y, transform.position.z);
            if (playerPos.position.y > transform.position.y + 0.5f)
            {
                targetPosition = new Vector3(targetPosition.x, targetPosition.y + 0.5f, targetPosition.z);
            }else if(playerPos.position.y < transform.position.y - 0.5f)
            {
                targetPosition = new Vector3(targetPosition.x, targetPosition.y - 0.5f, targetPosition.z);
            }
            if(playerPos.position.x > transform.position.x + 0.5f)
            {
                targetPosition = new Vector3(targetPosition.x + 1.25f, targetPosition.y, targetPosition.z);
            }else if(playerPos.position.x < transform.position.x - 0.5f)
            {
                targetPosition = new Vector3(targetPosition.x - 1.25f, targetPosition.y, targetPosition.z);
            }            
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);            
        }*/
        Vector3 currentPosition = transform.position;
        Vector3 targetPosition = new Vector3(Mathf.Clamp(playerPos.position.x, limitsMin.x, limitsMax.x), Mathf.Clamp(playerPos.position.y, limitsMin.y, limitsMax.y), currentPosition.z);
        transform.position = Vector3.Lerp(currentPosition, targetPosition, Time.deltaTime * smoothTime);
    }
    public void adaptBounds(Vector4 v4)
    {
        limitsMin.x = v4.x;
        limitsMin.y = v4.y;
        limitsMax.x = v4.z;
        limitsMax.y = v4.w;
        Debug.Log(limitsMax);
        Debug.Log(limitsMin);
    }
}
