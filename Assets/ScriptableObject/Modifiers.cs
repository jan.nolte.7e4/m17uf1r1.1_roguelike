﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Modifiers", menuName = "ScriptableObjects/Items/Modifiers", order = 1)]
public class Modifiers : Items
{
    [Serializable]
    public struct Mods
    {
        public ModifierType modifierType;
        public float value;
    }
    public List<Mods> listEffects;
    public enum ModifierType
    {
        DMGMultiplier, 
        DefenceMultiplier, 
        MovementSpeedBonus,
        CooldownDashReduc,
        MaxHPUp,
        HealingPowerUP,
    }
    public void effectOnPlayer(GameObject player)
    {
        foreach (Mods m in listEffects)
        {
            applyEffect(player, m.value, m.modifierType);
        }
    }
    private void applyEffect(GameObject player, float value, ModifierType effectType)
    {
        PlayerData pd = player.GetComponent<PlayerData>();
        MovementPlayer mp = player.GetComponent<MovementPlayer>();
        switch (effectType)
        {
            case ModifierType.CooldownDashReduc:
                mp.moreDashCooldown(value);
                break;
            case ModifierType.DMGMultiplier:
                pd.moreDMGMultiplayer(value);
                break;
            case ModifierType.DefenceMultiplier:
                pd.moreDMGReduction(value);
                break;
            case ModifierType.MovementSpeedBonus:
                mp.moreMovementSpeed(value);
                break;
            case ModifierType.MaxHPUp:
                pd.moreMaxHP(value);
                break;
            case ModifierType.HealingPowerUP:
                pd.moreHealingPower(value);
                break;
        }
    }
}
