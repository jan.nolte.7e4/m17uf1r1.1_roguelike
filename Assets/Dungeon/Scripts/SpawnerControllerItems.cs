﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerControllerItems : MonoBehaviour
{
    public List<GameObject> spwaners;
    private void Start()
    {
        StartCoroutine("Spawning");
    }
    private IEnumerator Spawning () {
        while (true)
        {
            foreach (GameObject spawner in spwaners)
            {
                if (Random.Range(0, 100) > 85)
                {
                    spawner.GetComponent<itemSpawner>().spawnItem();
                }
            }
            yield return new WaitForSeconds(2f);
        }
        
    }

}
