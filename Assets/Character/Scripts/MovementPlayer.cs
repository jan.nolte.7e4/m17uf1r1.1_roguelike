﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPlayer : MonoBehaviour
{
    private float characterSpeed;
    private float cooldownDash;
    private Rigidbody2D playerRig;
    private bool inmortal;
    private float spacePressed;
    void Start()
    {
        characterSpeed = this.gameObject.GetComponent<PlayerData>().initialInfo.movementStarting;
        cooldownDash = this.gameObject.GetComponent<PlayerData>().initialInfo.cooldownDash;
        playerRig = this.gameObject.GetComponent<Rigidbody2D>();
        inmortal = false;
        spacePressed = 0;
    }
    void Update()
    {
        fixAngularVelocity(20);
        float dashSpeed = 1;
        Vector3 directionCursor = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(directionCursor.y, directionCursor.x) * Mathf.Rad2Deg));
        
        if (Input.GetKey(KeyCode.Space) && (Time.time-spacePressed>=cooldownDash))
        {
            //Debug.Log(inmortal);
            dashSpeed = 5;
            if (inputMoving())
            {
                IEnumerator coroutine = DashInmortal(this.gameObject.GetComponent<PlayerData>().initialInfo.dashInvulnerability);
                StartCoroutine(coroutine);
            }
        }
        if (inputMoving())
        {
            Vector2 velocity = new Vector2();
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                velocity += Vector2.right;
            }
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                velocity += Vector2.left;
            }
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
            {
                velocity += Vector2.up;
            }
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
            {
                velocity += Vector2.down;
            }
            playerRig.velocity = velocity.normalized * characterSpeed/2 * dashSpeed;
        }
        else
        {
            playerRig.velocity = new Vector2(0, 0);
        }
        
    }
    private bool inputMoving()
    {
        return Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A);
    }
    private void fixAngularVelocity(float limit)
    {
        if (playerRig.angularVelocity < -limit)
        {
            playerRig.angularVelocity = -limit;
        }else if (playerRig.angularVelocity > limit)
        {
            playerRig.angularVelocity = limit;
        }
    }
    private IEnumerator DashInmortal(float waitTime)
    {
        inmortal = true;
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.blue;
        yield return new WaitForSeconds(waitTime);
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        inmortal = false;
        spacePressed = Time.time;
    }
    public bool isInmortal()
    {
        return inmortal;
    }
    public void moreMovementSpeed(float amount)
    {
        characterSpeed += amount;
    }
    public void moreDashCooldown(float amount)
    {
        cooldownDash += amount;
    }
    public string getDetails()
    {
        return "\nSpeed: " + characterSpeed.ToString("f1") + "\nC_Dash: " + cooldownDash.ToString("f1");
    }
}
