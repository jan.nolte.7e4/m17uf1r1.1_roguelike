﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "EnemyData", menuName = "ScriptableObjects/Entity/EnemyData", order = 1)]
public class EnemyData : EntityData
{
    public classEnemy enemyClass;
    public GameObject prefabBullet;
    public Color enemyColor;
    public int scoreEarn;
    public float dmgToPlayer;
    public float bulletSpeed;
    public float cadenceShoot;
    public enum classEnemy
    {
        Runner,
        Shooter
    }

}
