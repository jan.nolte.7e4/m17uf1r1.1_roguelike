﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityData : ScriptableObject
{
    public float movementStarting;
    public float hpStarting;
}
