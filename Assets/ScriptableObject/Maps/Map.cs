﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Map", menuName = "ScriptableObjects/Maps", order = 1)]
public class Map : ScriptableObject
{
    public string name;
    public float minX;
    public float minY;
    public float maxX;
    public float maxY;
    public Vector3 startPosition;
    public GameObject prefabMap;
}
